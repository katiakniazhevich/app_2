package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

//import androidx.annotation.NonNull;
//import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NumbersAdapter extends RecyclerView.Adapter<NumbersAdapter.NumberViewHolder> {
    //@NonNull

    private static int viewHolderCount;

    private static int number;

    private int numberItem;

    public NumbersAdapter (int numberOfItems) {
        numberItem = numberOfItems;
        //viewHolderCount = 0;

    }

    @Override
    public NumberViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.number_list_item;

        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(layoutIdForListItem, parent, false);

        NumberViewHolder viewHolder = new NumberViewHolder(view);
        // viewHolder.viewHolderIndex.setText("Title"+ viewHolderCount);
        //viewHolderCount++;
       return viewHolder;
    }

    @Override
    public void onBindViewHolder(NumberViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return numberItem;
    }

    class NumberViewHolder extends RecyclerView.ViewHolder {

        TextView listItemNumberView;
        TextView viewHolderIndex;
        //ImageView viewimg;

        public NumberViewHolder(View itemView) {
            super(itemView);

            listItemNumberView = itemView.findViewById(R.id.tv_number_item);
            viewHolderIndex = itemView.findViewById(R.id.tv_view_holder_number);
            //viewimg = itemView.findViewById(R.id.tv_number_img);
        }

        void bind(int listIndex) {
            listItemNumberView.setText("Description" +String.valueOf(listIndex));

            viewHolderIndex.setText("Title" +String.valueOf(listIndex));
        }
    }
}
